package ru.t1.strelcov.tm.repository.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.strelcov.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.strelcov.tm.dto.model.TaskDTO;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Scope("prototype")
public final class TaskDTORepository extends AbstractBusinessDTORepository<TaskDTO> implements ITaskDTORepository {

    @Autowired
    public TaskDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @NotNull
    public Class<TaskDTO> getClazz() {
        return TaskDTO.class;
    }

    @Override
    public void removeAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String jpql = "DELETE FROM " + getEntityName() + " e WHERE e.userId = :userId AND e.projectId = :projectId";
        entityManager.createQuery(jpql).setParameter("userId", userId).setParameter("projectId", projectId).executeUpdate();
    }

    @SneakyThrows
    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String jpql = "SELECT e FROM " + getEntityName() + " e WHERE e.userId = :userId AND e.projectId = :projectId";
        return entityManager.createQuery(jpql, getClazz()).setParameter("userId", userId).setParameter("projectId", projectId).getResultList();
    }

}
