package ru.t1.strelcov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.strelcov.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.strelcov.tm.dto.model.ProjectDTO;

import javax.persistence.EntityManager;

@Repository
@Scope("prototype")
public final class ProjectDTORepository extends AbstractBusinessDTORepository<ProjectDTO> implements IProjectDTORepository {

    @Autowired
    public ProjectDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @NotNull
    public Class<ProjectDTO> getClazz() {
        return ProjectDTO.class;
    }

}
