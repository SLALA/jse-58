package ru.t1.strelcov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.repository.model.IBusinessRepository;
import ru.t1.strelcov.tm.api.service.model.IBusinessService;
import ru.t1.strelcov.tm.enumerated.SortType;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.empty.EmptyNameException;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.exception.entity.EntityNotFoundException;
import ru.t1.strelcov.tm.exception.system.IncorrectSortOptionException;
import ru.t1.strelcov.tm.model.AbstractBusinessEntity;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public abstract class AbstractBusinessService<E extends AbstractBusinessEntity> extends AbstractService<E> implements IBusinessService<E> {

    @NotNull
    public abstract IBusinessRepository<E> getRepository(@NotNull final EntityManager entityManager);

    @SneakyThrows
    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IBusinessRepository<E> entityRepository = getRepository(entityManager);
        try {
            return entityRepository.findAll(userId);
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId, @Nullable final String sort) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(sort).orElseThrow(() -> {
            throw new IncorrectSortOptionException(sort);
        });
        if (!SortType.isValidByName(sort)) throw new IncorrectSortOptionException(sort);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IBusinessRepository<E> entityRepository = getRepository(entityManager);
        try {
            return entityRepository.findAll(userId, SortType.valueOf(sort).getDataBaseName());
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void clear(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IBusinessRepository<E> entityRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            entityRepository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @NotNull
    @Override
    public E findById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IBusinessRepository<E> entityRepository = getRepository(entityManager);
        try {
            return Optional.ofNullable(entityRepository.findById(userId, id)).orElseThrow(EntityNotFoundException::new);
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @NotNull
    @Override
    public E findByName(@Nullable final String userId, @Nullable final String name) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IBusinessRepository<E> entityRepository = getRepository(entityManager);
        try {
            return Optional.ofNullable(entityRepository.findByName(userId, name)).orElseThrow(EntityNotFoundException::new);
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @NotNull
    @Override
    public E removeById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IBusinessRepository<E> entityRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final E entity = Optional.ofNullable(entityRepository.removeById(userId, id)).orElseThrow(EntityNotFoundException::new);
            entityManager.getTransaction().commit();
            return entity;

        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @NotNull
    @Override
    public E removeByName(@Nullable final String userId, @Nullable final String name) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IBusinessRepository<E> entityRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final E entity = Optional.ofNullable(entityRepository.removeByName(userId, name)).orElseThrow(EntityNotFoundException::new);
            entityManager.getTransaction().commit();
            return entity;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @NotNull
    @Override
    public E updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IBusinessRepository<E> entityRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final E entity = Optional.ofNullable(entityRepository.findById(userId, id)).orElseThrow(EntityNotFoundException::new);
            entity.setName(name);
            entity.setDescription(description);
            entityRepository.update(entity);
            entityManager.getTransaction().commit();
            return entity;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @NotNull
    @Override
    public E updateByName(@Nullable final String userId, @Nullable final String oldName, @Nullable final String name, @Nullable final String description) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(oldName).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IBusinessRepository<E> entityRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final E entity = Optional.ofNullable(entityRepository.findByName(userId, oldName)).orElseThrow(EntityNotFoundException::new);
            entity.setName(name);
            entity.setDescription(description);
            entityRepository.update(entity);
            entityManager.getTransaction().commit();
            return entity;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @NotNull
    @Override
    public E changeStatusById(@Nullable final String userId, @Nullable final String id, @NotNull final Status status) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IBusinessRepository<E> entityRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final E entity = Optional.ofNullable(entityRepository.findById(userId, id)).orElseThrow(EntityNotFoundException::new);
            entity.setStatus(status);
            if (status == Status.IN_PROGRESS)
                entity.setDateStart(new Date());
            entityRepository.update(entity);
            entityManager.getTransaction().commit();
            return entity;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @NotNull
    @Override
    public E changeStatusByName(@Nullable final String userId, @Nullable final String name, @NotNull final Status status) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IBusinessRepository<E> entityRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final E entity = Optional.ofNullable(entityRepository.findByName(userId, name)).orElseThrow(EntityNotFoundException::new);
            entity.setStatus(status);
            if (status == Status.IN_PROGRESS)
                entity.setDateStart(new Date());
            entityRepository.update(entity);
            entityManager.getTransaction().commit();
            return entity;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
