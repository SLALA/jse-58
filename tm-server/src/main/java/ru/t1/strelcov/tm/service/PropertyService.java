package ru.t1.strelcov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.strelcov.tm.api.service.IPropertyService;

@Getter
@PropertySource("classpath:application.properties")
@Service
public class PropertyService implements IPropertyService {

    @Value("${password.secret}")
    public String passwordSecret;

    @Value("${password.iteration:1}")
    public Integer passwordIteration;

    @Value("${server.host:localhost}")
    public String serverHost;

    @Value("${server.port:8080}")
    public Integer serverPort;

    @Value("${session.secret}")
    public String sessionSecret;

    @Value("${session.timeout:5}")
    public Integer sessionTimeout;

    @Value("${backup:disabled}")
    public String backupStatus;

    @Value("${init.data:disabled}")
    public String initDataStatus;

    @Value("${hibernate.log_level:ALL}")
    public String hibernateLogLevel;

    @Value("${persistence.unit:ENTERPRISE}")
    public String persistenceUnit;

    @Value("${mq.connection.factory:tcp://localhost:61616}")
    public String MQConnectionFactory;

    @NotNull
    @Override
    public String getName() {
        return Manifests.read("developer");
    }

    @NotNull
    @Override
    public String getVersion() {
        return Manifests.read("build");
    }

    @NotNull
    @Override
    public String getEmail() {
        return Manifests.read("email");
    }

}