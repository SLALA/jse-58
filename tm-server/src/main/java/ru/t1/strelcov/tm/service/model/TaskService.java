package ru.t1.strelcov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.strelcov.tm.api.repository.model.IProjectRepository;
import ru.t1.strelcov.tm.api.repository.model.ITaskRepository;
import ru.t1.strelcov.tm.api.repository.model.IUserRepository;
import ru.t1.strelcov.tm.api.service.model.ITaskService;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.empty.EmptyNameException;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.exception.entity.EmptyProjectIdException;
import ru.t1.strelcov.tm.exception.entity.EmptyTaskIdException;
import ru.t1.strelcov.tm.exception.entity.EntityNotFoundException;
import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.model.Task;
import ru.t1.strelcov.tm.model.User;
import ru.t1.strelcov.tm.repository.model.ProjectRepository;
import ru.t1.strelcov.tm.repository.model.TaskRepository;
import ru.t1.strelcov.tm.repository.model.UserRepository;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@Service
public final class TaskService extends AbstractBusinessService<Task> implements ITaskService {

    @NotNull
    public ITaskRepository getRepository(@NotNull final EntityManager entityManager) {
        return context.getBean(TaskRepository.class, entityManager);
    }

    @NotNull
    public EntityManager getEntityManager() {
        return context.getBean(EntityManager.class);
    }

    @NotNull
    public IUserRepository getUserRepository(@NotNull final EntityManager entityManager) {
        return context.getBean(UserRepository.class, entityManager);
    }

    @NotNull
    public IProjectRepository getProjectRepository(@NotNull final EntityManager entityManager) {
        return context.getBean(ProjectRepository.class, entityManager);
    }

    @NotNull
    @Override
    public Task add(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository taskRepository = getRepository(entityManager);
        @NotNull final IUserRepository userRepository = getUserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final User user = Optional.ofNullable(userRepository.findById(userId)).orElseThrow(EntityNotFoundException::new);
            @NotNull final Task task;
            if (Optional.ofNullable(description).filter((i) -> !i.isEmpty()).isPresent())
                task = new Task(user, name, description);
            else
                task = new Task(user, name);
            taskRepository.add(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @NotNull
    @Override
    public List<Task> findAllTasksByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(projectId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyProjectIdException::new);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository taskRepository = getRepository(entityManager);
        try {
            return taskRepository.findAllByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @NotNull
    @Override
    public Task bindTaskToProject(@Nullable final String userId, @Nullable final String taskId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(taskId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyTaskIdException::new);
        Optional.ofNullable(projectId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyProjectIdException::new);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository taskRepository = getRepository(entityManager);
        @NotNull final IProjectRepository projectRepository = getProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final Task task = Optional.ofNullable(taskRepository.findById(userId, taskId)).orElseThrow(EntityNotFoundException::new);
            @NotNull final Project project = Optional.ofNullable(projectRepository.findById(userId, projectId)).orElseThrow(EntityNotFoundException::new);
            task.setProject(project);
            taskRepository.update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @NotNull
    @Override
    public Task unbindTaskFromProject(@Nullable final String userId, @Nullable final String taskId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(taskId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository taskRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final Task task = Optional.ofNullable(taskRepository.findById(userId, taskId)).orElseThrow(EntityNotFoundException::new);
            task.setProject(null);
            taskRepository.update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
