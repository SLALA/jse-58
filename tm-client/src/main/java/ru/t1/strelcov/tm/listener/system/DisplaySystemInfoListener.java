package ru.t1.strelcov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.event.ConsoleEvent;
import ru.t1.strelcov.tm.listener.AbstractListener;
import ru.t1.strelcov.tm.util.NumberUtil;

@Component
public final class DisplaySystemInfoListener extends AbstractListener {

    @NotNull
    @Override
    public String arg() {
        return "-i";
    }

    @NotNull
    @Override
    public String name() {
        return "info";
    }

    @NotNull
    @Override
    public String description() {
        return "Display system info.";
    }

    @Override
    @EventListener(condition = "@displaySystemInfoListener.name() == #event.name || @displaySystemInfoListener.arg() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

}
