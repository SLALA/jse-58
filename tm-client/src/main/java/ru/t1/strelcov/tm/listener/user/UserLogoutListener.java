package ru.t1.strelcov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.dto.request.UserLogoutRequest;
import ru.t1.strelcov.tm.event.ConsoleEvent;

@Component
public final class UserLogoutListener extends AbstractUserListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @NotNull
    @Override
    public String description() {
        return "Logout.";
    }

    @Override
    @EventListener(condition = "@userLogoutListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[LOGOUT]");
        authEndpoint.logout(new UserLogoutRequest(getToken()));
    }

}
